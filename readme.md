## Smile Direct Club Assessment Submission
> By Tom Leslie

For this submission I chose to use a simple HTML and CSS solution. In an ideal environment I would have used something like PostCSS for variables and some quality of life improvements, but overall it came out nice and clean.

Just open the `index.html` file in a browser and voila!